package com.study.demo.camel;

import org.apache.camel.builder.RouteBuilder;

public class SimpleCamelCallRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {
//		from("flowable:camelprocess:simpleCall").to("log:com.study.demo.camel");
		from("flowable:camelprocess:simpleCall").transform().simple("${property.input} World");
		
	}

}
