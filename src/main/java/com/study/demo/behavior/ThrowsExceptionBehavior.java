package com.study.demo.behavior;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.DelegateHelper;
import org.flowable.engine.impl.delegate.ActivityBehavior;

public class ThrowsExceptionBehavior implements ActivityBehavior {

	private static final long serialVersionUID = 1L;
	
	public void execute(DelegateExecution execution) {
	    String var = (String) execution.getVariable("exception");

	    String sequenceFlowToTake = null;
	    try {
	      executeLogic(var);
	      sequenceFlowToTake = "no-exception";
	    } catch (Exception e) {
	      sequenceFlowToTake = "exception";
	    }
	    DelegateHelper.leaveDelegate(execution, sequenceFlowToTake);
	  }
	protected void executeLogic(String value) {
        if (value.equals("throw-exception")) {
            throw new RuntimeException();
        }
    }
}
