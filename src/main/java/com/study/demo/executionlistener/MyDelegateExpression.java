package com.study.demo.executionlistener;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.DelegateHelper;
import org.flowable.engine.delegate.JavaDelegate;

public class MyDelegateExpression implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		FlowElement currentFlowElement = DelegateHelper.getFlowElement(execution);
        System.out.println("=========================【执行器，当前节点】：" + currentFlowElement.getName() + "=========================");

	}

}
