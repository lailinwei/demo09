package com.study.demo.delegate;

import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class ClassImplementsJavaDelegate implements JavaDelegate {
	private Expression text;
	public void execute(DelegateExecution execution) {
		System.out.println("=====================指定实现了JavaDelegate或ActivityBehavior的类=====================");
		execution.setVariable("var", ((String)text.getValue(execution)).toUpperCase());
		
	}

}
