package com.study.demo.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.study.demo.config.BaseConfiguation;


/**
 *Http任务--测试
 *
 */
public class HttpTaskTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("httptaskprocess")
													.name("httptaskprocess")
													.addClasspathResource("process/Http任务.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例 
	 * 
	 */
	@Test
	public void start() {
		String processDefinitionKey = "httptaskprocess";
    	

        Map<String, Object> vars = new HashMap<String, Object>();
        vars.put("requestTimeout", 6000);
        
		runtimeService.startProcessInstanceByKey(processDefinitionKey,vars);
	}
	
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "105022";
		taskService.complete(taskId);
	}
}
