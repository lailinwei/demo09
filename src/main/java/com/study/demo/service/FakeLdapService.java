package com.study.demo.service;

import java.util.Arrays;
import java.util.List;

/**
 *分配用户
 *
 */
public class FakeLdapService {
	public String findManagerForEmployee(String employee) {
		return employee + ".org";
	}

	public List<String> findAllSales() {
		return Arrays.asList("kermit", "gonzo", "fozzie");
	}
}
