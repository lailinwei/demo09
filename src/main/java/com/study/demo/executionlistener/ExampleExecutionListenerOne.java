package com.study.demo.executionlistener;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;

public class ExampleExecutionListenerOne implements ExecutionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void notify(DelegateExecution execution) {
        System.out.println("=========================执行器,流程启动时执行=========================");
		execution.setVariable("variableSetInExecutionListener", "firstValue");
		execution.setVariable("eventNameReceived", execution.getEventName());
		execution.setVariable("businessKeyInExecution", execution.getProcessInstanceBusinessKey());

	}

}
