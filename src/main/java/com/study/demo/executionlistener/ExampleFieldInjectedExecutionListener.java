package com.study.demo.executionlistener;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.DelegateHelper;
import org.flowable.engine.delegate.ExecutionListener;

public class ExampleFieldInjectedExecutionListener implements ExecutionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Expression fixedValue;
	private Expression dynamicValue;
	public void notify(DelegateExecution execution) {
		FlowElement currentFlowElement = DelegateHelper.getFlowElement(execution);
        System.out.println("=========================【执行器，当前节点】：" + currentFlowElement.getName() + "=========================");
		execution.setVariable("dynamicValue", fixedValue.getValue(execution).toString() +
		dynamicValue.getValue(execution).toString());
	}

}
